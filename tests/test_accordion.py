# test.py
import unittest
from xml.etree.ElementTree import fromstring, tostring

import markdown
from bs4 import BeautifulSoup

from dsfr_structure.extension.accordion import DsfrAccordionExtension


def normalize_html(html: str) -> str:
    return tostring(fromstring(html)).decode()


def remove_whitespaces_and_indentations(html):
    soup = BeautifulSoup(html, "html.parser")
    return soup.prettify()


class TestAdmonitionsExtension(unittest.TestCase):

    def setUp(self):
        self.md = markdown.Markdown(extensions=[DsfrAccordionExtension()])

    def test_case1(self):
        # given
        test_case = """
/// accordion | Titre
Lorem ipsum dolor.
///"""

        expected_output = """
<section class="fr-accordion">
    <h3 class="fr-accordion__title">
        <button class="fr-accordion__btn" aria-expanded="false" aria-controls="accordion-0">Titre</button>
    </h3>
    <div class="fr-collapse" id="accordion-0">
        <p>
        Lorem ipsum dolor.
        </p>
    </div>
</section>"""

        # when
        html_output = self.md.convert(test_case)

        html_output = remove_whitespaces_and_indentations(html_output)
        expected_output = remove_whitespaces_and_indentations(expected_output)

        # then
        self.assertEqual(expected_output, html_output)


if __name__ == "__main__":
    unittest.main()
